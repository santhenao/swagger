package com.techu.swagger.controller;

import com.techu.swagger.model.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ProductsController {

    private ArrayList<Producto> listProducts;

    public ProductsController(){
        listProducts = new ArrayList<>();
        listProducts.add(new Producto("1","panela", 122.2, "categoria 1"));
        listProducts.add(new Producto("2","yuca", 131.99, "categoria 2"));
        listProducts.add(new Producto("3","papa", 199.0, "categoria 3"));
    }

    @GetMapping("/products")
    public ResponseEntity<ArrayList<Producto>> getProducts (){
        return new ResponseEntity<>(listProducts, HttpStatus.OK);
    }

}
