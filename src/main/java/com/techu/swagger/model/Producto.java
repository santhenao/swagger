package com.techu.swagger.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Producto {

    @ApiModelProperty(required = true, value = "Id producto", dataType = "String",  example = "1", position = 1)
    private String id;

    @ApiModelProperty(required = true, value = "Nombre producto", dataType = "String",  example = "panela", position = 2)
    private String nombre;

    @ApiModelProperty(required = true, value = "Precio producto", dataType = "Double",  example = "11.0", position = 3)
    private double precio;

    @ApiModelProperty(required = true, value = "Categoria producto", dataType = "String",  example = "categoria 1", position = 4)
    private String categoria;

    public Producto(String id, String nombre, double precio, String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
